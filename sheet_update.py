#!usr/bin/python3

import gspread
from oauth2client.service_account import ServiceAccountCredentials
from write_mysql import WriteToMySQLTable,GetSpreadsheetData,PreserveNULLValues


def main():
    # define the scope
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']

    # add credentials to the account
    creds = ServiceAccountCredentials.from_json_keyfile_name('Sheetupdate-02b6356b8986.json', scope)

    # authorize the clientsheet
    client = gspread.authorize(creds)


    data = GetSpreadsheetData("Project Pragati", 0,client)


    PreserveNULLValues(data)
    WriteToMySQLTable(data,'pragati_retailers')

if __name__=='__main__':
    main()